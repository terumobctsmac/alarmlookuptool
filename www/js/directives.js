//show toTop
app.directive('scrollposition', function ($ionicScrollDelegate) {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {

            element.on('scroll', function () {
                if ($ionicScrollDelegate.$getByHandle("alarmList").getScrollPosition().top > 300) {
                    angular.element(document.getElementsByClassName("toTop")[0]).addClass("toTopShow");
                } else {
                    angular.element(document.getElementsByClassName("toTop")[0]).removeClass("toTopShow");
                }
            });

            scope.$on('scrolltotop', function () {
                $ionicScrollDelegate.scrollTop(true);
            });
        }
    };
});

//calculate height of header
app.directive('searchheader', function () {
    return {
        restrict: 'A',
        link: function (scope, element, attr) {
            var customContentEl = angular.element(document.getElementsByClassName("customContent")[0]);
            scope.$on('hideheader', function () {
                customContentEl.css("transform", "translateY(" + -(element[0].clientHeight + 1) + "px)");
            });

            scope.$on('showheader', function () {
                customContentEl.css("transform", "translateY(0px)");
            });
        }
    };
});

//calculate height of header
app.directive('alarmheader', function () {
    return {
        restrict: 'A',
        link: function (scope, element, attr) {
            angular.element(document.getElementsByClassName("alarmInfo")[0]).css("top", element[0].clientHeight + "px");
        }
    };
});