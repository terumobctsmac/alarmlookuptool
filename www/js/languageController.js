app.controller('languageCtrl', function ($scope, $http, sharedData, $state, languageRequest, $rootScope, $window) {

    //get objects    
    $scope.productObj = sharedData.getProductObj();
    $scope.versionObj = sharedData.getVersionObj();

    //request languages, the http.get is happening in the factory
    languageRequest.request($scope.productObj.shortName, $scope.versionObj.shortName).success(function (response) {
        $rootScope.languages = response.langList;
        $window.localStorage.setItem('languages', JSON.stringify($rootScope.languages));
    });

    //user click on language
    $scope.langClick = function (i, x) {
        sharedData.setLanguageObj(x);
        sharedData.setSearchVal('');
        $state.go('search');
    };
});