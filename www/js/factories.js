app.factory("languageRequest", function ($http) {
    return {
        request: function (p, v) {

            //request language list from json
            return $http.get(p.concat("/", v, "/", v, "_languages.json")).success(function (response) {
                // loop over langList to add index 
                angular.forEach(response.langList, function (v, i) {
                    v.langIndex = i;
                });
            });
        }
    };
});

app.factory("alarmRequest", function ($http, sharedData, $q) {
    return {
        request: function () {

            var verObjShortName = sharedData.getVersionObj().shortName;

            var langObjLangCode = sharedData.getLanguageObj().langCode;

            //request the alarms
            var p1 = $http.get(sharedData.getProductObj().shortName.concat("/", verObjShortName, "/", verObjShortName, "_alarms_", langObjLangCode, ".json"));

            //request static strings
            var p2 = $http.get("data/strings_".concat(langObjLangCode, ".json"));

            //merge the two and return them
            return $q.all([p1, p2]).then(function (data) {

                var currentAlarmObj = data[0].data.alarms.filter(function (x) {
                    return x.alarmId == sharedData.getAlarmId();
                });

                return [currentAlarmObj[0], data[1].data.strings];
            });
        }
    };
});