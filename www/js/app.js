var app = angular.module('starter', ['ionic', 'ngIOS9UIWebViewPatch'])
    .run(function ($rootScope, $ionicModal, $window, sharedData, $state, $http, alarmRequest) {

        //load language array if it exists
        $rootScope.languages = null;
        if ($window.localStorage.getItem("languages") !== null) {
            $rootScope.languages = JSON.parse($window.localStorage.getItem("languages"));
        }

        //search toggles
        $rootScope.searchFilters = {
            optia: [
                {
                    name: "prompt",
                    value: "Prompt",
                    checked: false
            }, {
                    name: "safety",
                    value: "_",
                    checked: false
            }]
        };

        //go back to products
        $rootScope.backProd = function () {
            $state.go("products");
        };

        //go back to versions
        $rootScope.backVer = function () {
            $state.go("versions");
        };

        //go back to languages
        $rootScope.backLang = function () {
            $state.go("languages");
        };

        //go back to search
        $rootScope.backSearch = function () {
            $state.go("search");
        };

        //requesting alarms
        $rootScope.requestedLangName = null;
        $rootScope.requestedAlarmNames = null;
        $rootScope.limitNumber = 50;

        //assign recently viewed modal
        $ionicModal.fromTemplateUrl('recently-viewed.html', {
            scope: $rootScope
        }).then(function (modal) {
            $rootScope.modal = modal;
        });

        //show recently viewed modal
        $rootScope.recentlyViewed = function () {
            $rootScope.modal.show();
        };

        //hide recently viewed
        $rootScope.hideViewed = function () {
            $rootScope.modal.hide();
        };

        //go to recently viewed alarm click
        $rootScope.gotoAlarm = function (alarm) {
            sharedData.setProductObj(alarm.product);
            sharedData.setLanguageObj(alarm.language);
            sharedData.setVersionObj(alarm.version);
            sharedData.setAlarmId(alarm.alarmId);
            sharedData.setEnumerator(alarm.enumerator);
            $rootScope.modal.hide();
            $state.go('alarm', {
                recentlyViewed: true
            });
        };

        //recently viewed clear button
        $rootScope.clear = function () {
            $window.localStorage.clear();
            $window.localStorage.setItem('recentAlarms', JSON.stringify([]));
            $rootScope.localAlarms = null;
        };

        //get recently viewed from localStorage
        $rootScope.localAlarms = null;
        if ($window.localStorage.getItem("recentAlarms") === null) {
            $window.localStorage.setItem('recentAlarms', JSON.stringify([]));
        } else {
            $rootScope.localAlarms = JSON.parse($window.localStorage.getItem("recentAlarms"));

            //set language to last viewed alarm
            if ($rootScope.localAlarms.length > 0) {
                sharedData.setLanguageObj($rootScope.localAlarms[0].language);
            }
        }

        //function to see if the array contains an object
        $rootScope.containsObject = function (obj, list) {
            for (var i = 0; i < list.length; i++) {
                if (angular.equals(list[i], obj)) {
                    return true;
                }
            }
            return false;
        };

        //change language select function
        $rootScope.currentAlarm = null;
        $rootScope.changeLang = function (l, isAlarms) {
            if (!l) {
                return;
            }

            //set lang obj
            sharedData.setLanguageObj(l);

            //reset search val
            sharedData.setSearchVal('');

            //set selected language
            $rootScope.selectedLanguage = $rootScope.languages[l.langIndex];

            //if it is an alarm, request it
            if (isAlarms) {
                alarmRequest.request().then(function (response) {
                    $rootScope.currentAlarm = response;
                });

            } else {

                //if it is the alarm list
                $http.get(sharedData.getProductObj().shortName.concat("/", sharedData.getVersionObj().shortName, "/", sharedData.getVersionObj().shortName, "_alarmNames_", sharedData.getLanguageObj().langCode, ".json")).success(function (response) {
                    $rootScope.alarmNames = response.nameList;
                });
            }
        };

    })
    .config(function ($stateProvider, $urlRouterProvider) {
        $stateProvider
            .state('products', {
                url: '/products',
                templateUrl: 'templates/products.html',
                controller: 'prodCtrl'
            })
            .state('versions', {
                url: '/versionView',
                templateUrl: 'templates/versions.html',
                controller: 'versionCtrl'
            })
            .state('languages', {
                url: "/languageView",
                templateUrl: "templates/languages.html",
                controller: "languageCtrl"
            })
            .state('search', {
                url: "/searchView",
                templateUrl: "templates/search.html",
                controller: "searchCtrl"
            })
            .state('alarm', {
                url: "/alarmText",
                templateUrl: "templates/tab-alarm.html",
                controller: "alarmCtrl",
                params: {
                    recentlyViewed: false
                }
            });

        $urlRouterProvider.otherwise('/products');
    });