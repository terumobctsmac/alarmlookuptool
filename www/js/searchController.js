app.controller('searchCtrl', function ($scope, $http, sharedData, $state, $timeout, $ionicScrollDelegate, $rootScope, $ionicModal, languageRequest, $window) {

    //get objects in view
    $scope.productObj = sharedData.getProductObj();
    $scope.versionObj = sharedData.getVersionObj();
    $scope.languageObj = sharedData.getLanguageObj();

    //request the language list
    languageRequest.request($scope.productObj.shortName, $scope.versionObj.shortName).success(function (response) {
        $rootScope.languages = response.langList;
        $window.localStorage.setItem('languages', JSON.stringify($rootScope.languages));
        $rootScope.selectedLanguage = $rootScope.languages[$scope.languageObj.langIndex];
    });

    //get search value
    $scope.search = sharedData.getSearchVal();

    //request alarm names if languages havent changed
    if ($rootScope.requestedLangName !== $scope.languageObj.langName) {

        //reset limit
        $rootScope.limitNumber = 50;

        //re-request data
        $http.get($scope.productObj.shortName.concat("/", $scope.versionObj.shortName, "/", $scope.versionObj.shortName, "_alarmNames_", $scope.languageObj.langCode, ".json")).success(function (response) {

            //assign new alarms
            $rootScope.alarmNames = response.nameList;

            //set scroll top
            $rootScope.$watch('alarmNames', function () {
                sharedData.setScrollPos(0);
                $ionicScrollDelegate.scrollTo(0, 0);
            });

            //assign new language
            $rootScope.requestedLangName = $scope.languageObj.langName;

        });
    } else {

        //get scroll position
        $scope.scrollPos = sharedData.getScrollPos() || 0;

        //scroll to position
        $rootScope.$watch('alarmNames', function () {
            $ionicScrollDelegate.scrollTo(0, $scope.scrollPos);
        });

    }

    //click on each alarm
    $scope.alarmClick = function (x, s) {

        //set enumerator and alarm id and alarm name
        sharedData.setEnumerator(x.enumerator);
        sharedData.setAlarmId(x.alarmId);
        sharedData.setAlarmName(x.name);

        //set search val
        sharedData.setSearchVal(s);

        //set scroll pos
        sharedData.setScrollPos($ionicScrollDelegate.$getByHandle("alarmList").getScrollPosition().top);

        //go to alarm view
        $state.go('alarm');
    };

    //limit json
    $scope.loadMore = function () {
        var t = $timeout(function () {
            $rootScope.limitNumber = $rootScope.limitNumber + 50;
            $scope.$broadcast('scroll.infiniteScrollComplete');
            $timeout.cancel(t);
        }, 1000);
    };

    //settings modal
    $ionicModal.fromTemplateUrl('settings.html', {
        scope: $scope,
        animation: 'slide-in-down'
    }).then(function (modal) {
        $scope.settingsModal = modal;
    });

    $scope.showSettings = function () {
        $scope.settingsModal.show();
    };

    $scope.hideSettings = function () {
        $scope.settingsModal.hide();
    };

});