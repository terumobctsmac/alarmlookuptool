app.controller('alarmCtrl', function ($scope, $http, sharedData, $location, $state, $stateParams, $window, languageRequest, $rootScope, alarmRequest, $ionicScrollDelegate) {

    //load data
    $scope.productObj = sharedData.getProductObj();
    $scope.versionObj = sharedData.getVersionObj();
    $scope.languageObj = sharedData.getLanguageObj();

    //alarm name
    $scope.stringAlarmName = sharedData.getAlarmName();

    //get correct alarm troubleshooting
    $scope.stringEnumerator = sharedData.getEnumerator();
    //$scope.stringTsId = sharedData.getTSID();

    //alarm filter
    $scope.stringAlarmId = sharedData.getAlarmId();

    //lang index
    $rootScope.selectedLanguage = $rootScope.languages[$scope.languageObj.langIndex];

    //request current alarm
    alarmRequest.request().then(function (response) {
        $rootScope.currentAlarm = response;
    });

    //get trouble shooting
    $http.get($scope.productObj.shortName.concat("/", $scope.productObj.shortName, "_troubleshooting.json")).success(function (data) {
        $scope.troubleshooting = data.troubleshooting;
    });

    //get occurs
    $http.get($scope.productObj.shortName.concat("/", $scope.productObj.shortName, "_occurs.json")).success(function (data) {
        $scope.occurs = data.occursList;
    });

    //active tab on load
    $scope.activeTabIndex = 0;

    //click to go to the alarm slide
    $scope.clickAlarm = function () {
        $scope.activeTabIndex = 0;
    };

    //get info button
    $scope.clickInfo = function () {
        $scope.activeTabIndex = 1;

    };

    //click to go to trouble view
    $scope.clickTrouble = function () {
        $scope.activeTabIndex = 2;
    };

    //slide change listener
    $scope.slideHasChanged = function (x) {
        $ionicScrollDelegate.scrollTop();
        $scope.activeTabIndex = x;
    };


    //if we haven't gotten to this view by way of recently viewed, store the alarm in recently viewed
    if (!$stateParams.recentlyViewed) {

        //get the array
        var localAlarmArray = JSON.parse($window.localStorage.getItem("recentAlarms"));

        //create the new obj
        var obj = {
            product: $scope.productObj,
            version: $scope.versionObj,
            language: $scope.languageObj,
            alarmName: $scope.stringAlarmName,
            alarmId: $scope.stringAlarmId,
            enumerator: $scope.stringEnumerator
        };

        // if object doesn't exist, push new object into the array
        if (!$rootScope.containsObject(obj, localAlarmArray)) {

            //push it to the front
            localAlarmArray.unshift(obj);
            
            // keeping the 10 most recent alarms
            if (localAlarmArray.length >= 10) {
                localAlarmArray.pop();
            }

            //set items recent alarm
            $window.localStorage.setItem('recentAlarms', JSON.stringify(localAlarmArray));
        }

        //set the localAlarms
        $rootScope.localAlarms = localAlarmArray;

    }
});