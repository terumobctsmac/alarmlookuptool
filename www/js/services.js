app.service('sharedData', function () {
    var searchString = '';
    var stringEnumerator = null;
    var stringAlarmId = null;
    var stringTsId = null;
    var scrollPos = null;
    var stringAlarmName = null;

    //object to store product info
    var productObj = null;

    //object to store version info
    var versionObj = null;

    //object to store language info
    var languageObj = {
        langName: null,
        langCode: null,
        langIndex: null
    };

    //object to store the current alarm
    var currentAlarmObj = null;

    return {
        getEnumerator: function () {
            return stringEnumerator;
        },
        setEnumerator: function (value) {
            stringEnumerator = value;
        },
        getAlarmId: function () {
            return stringAlarmId;
        },
        setAlarmId: function (value) {
            stringAlarmId = value;
        },
        getSearchVal: function () {
            return searchString;
        },
        setSearchVal: function (value) {
            searchString = value;
        },
        getTSID: function () {
            return stringTsId;
        },
        setTSID: function (value) {
            stringTsId = value;
        },
        setScrollPos: function (value) {
            scrollPos = value;
        },
        getScrollPos: function () {
            return scrollPos;
        },

        //set product object
        setProductObj: function (value) {
            productObj = value;
        },

        getProductObj: function () {
            return productObj;
        },

        //set version object
        setVersionObj: function (value) {
            versionObj = value;
        },

        //get version object
        getVersionObj: function () {
            return versionObj;
        },

        //set language object
        setLanguageObj: function (value) {
            languageObj = value;

            //reset search on language change
            searchString = '';

        },

        //get language object
        getLanguageObj: function () {
            return languageObj;
        },

        setAlarmName: function (value) {
            stringAlarmName = value;
        },
        getAlarmName: function () {
            return stringAlarmName;
        }
    };
});