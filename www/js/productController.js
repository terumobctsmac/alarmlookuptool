app.controller('prodCtrl', function ($scope, $http, sharedData, $state) {

    //initial ajax json request
    $http.get('data/products.json').success(function (response) {
        $scope.products = response.prodList;
    });

    //each product click event
    $scope.prodClick = function (x) {
        
        //set product names
        sharedData.setProductObj(x);
        $state.go('versions');
    };
});