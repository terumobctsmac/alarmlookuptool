app.controller('versionCtrl', function ($scope, $http, sharedData, $state) {

    //get the product obj    
    $scope.productObj = sharedData.getProductObj();

    //request json for versions
    $http.get($scope.productObj.shortName.concat("/", $scope.productObj.shortName, "_versions.json")).success(function (response) {
        $scope.versions = response.verList;
    });

    //user click on version
    $scope.verClick = function (x) {

        //set version names
        sharedData.setVersionObj(x);

        //if lang code is not set, goto languageView otherwise goto searchView
        if (sharedData.getLanguageObj().langCode === null) {
            $state.go('languages');
        } else {
            $state.go('search');
        }
    };

});