// convert /n to <br>
app.filter("nl2br", function () {
    return function (data) {
        if (!data) return data;
        return data.replace(/\n\r?/g, '<br>');
    };
});

// filter by search strings
app.filter("alarmNameFilter", function ($filter) {
    return function (alarms, searchFilters) {
        var f = [];
        angular.forEach(searchFilters, function (v2) {
            if (!v2.checked) {
                f.push($filter('filter')(alarms, {
                    enumerator: v2.value
                }));
            }
        });
        angular.forEach(f, function (v1) {
            angular.forEach(v1, function (v2) {
                if (alarms.indexOf(v2) > -1) {
                    alarms.splice(alarms.indexOf(v2), 1);
                }
            });
        });
        if (alarms) {
            return alarms;
        }
    };
});